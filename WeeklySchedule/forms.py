from django import forms
from .models import Schedule

class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule
        exclude = ['user']

    activity = forms.CharField(label='Activity', max_length=75, widget=forms.TextInput(attrs={'placeholder' : 'Type your activity here'}))
    hour = forms.CharField(label='Hour', max_length=75, widget=forms.TextInput(attrs={'placeholder' : 'Type your hour here'}))
    detail = forms.CharField(label='Detail', max_length=150, widget=forms.TextInput(attrs={'placeholder' : 'Type your detail here'}))
    message = forms.CharField(label='Message', max_length=110, widget=forms.TextInput(attrs={'placeholder' : 'Type your message here'}))