from django.contrib.auth.models import User
from django.conf import settings
from django.db import models

# Create your models here.

class Schedule(models.Model):
    monday = models.BooleanField(default=False)
    tuesday = models.BooleanField(default=False)
    wednesday = models.BooleanField(default=False)
    thursday = models.BooleanField(default=False)
    friday = models.BooleanField(default=False)
    saturday = models.BooleanField(default=False)
    sunday = models.BooleanField(default=False)

    activity = models.CharField(max_length=75, default='')
    hour = models.TimeField(null=True)
    detail = models.CharField(max_length=150, default='')
    message = models.CharField(max_length=110, default='')

    user = models.ForeignKey(User, null=True, on_delete= models.CASCADE)