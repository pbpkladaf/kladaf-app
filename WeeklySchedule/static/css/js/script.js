$(".link-delete").on("click", function(e) {
  e.preventDefault();
  var $this = $(this);
  if(confirm("Sure to delete?")) {
    $.ajax ({
      url: "/weekly-schedule/json",
      type: "GET",
      dataType: "json",
      success: function(resp) {
        if (resp.message == 'success') {
          $this.parent('.record').fadeOut("slow", function() {
            $this.parent('.record').remove();
          });
        }
        else {
          alert(resp.message);
        }
      },
      error: function(resp) {
        console.log("Something went wrong");
      }
    });
  }
  return false;
});

function deleteRequest(day, elementContainingId) {
  // Excessive array manipulating coming
  let id = $(elementContainingId).html().split('').splice(20)[0]
  
  // String manipulation to obtain CSRF for POST Request
  let csrfRaw = $("csrfmiddlewaretoken").prevObject[0].cookie; // "csrftoken=token"
  let csrfmiddlewaretoken = csrfRaw.split('=')[1]; // token
  let formData = new FormData(); 
  
  formData.append('csrfmiddlewaretoken',csrfmiddlewaretoken);
  formData.append('day', day);
  formData.append('id', id);
  
  let url = `/weekly-schedule/${day}/${id}`
  
  $.ajax({
    url,
    type: 'POST',
    enctype: 'multipart/form-data',
    data: formData,
    success: function(r) { console.log(r) },
    error: function(e) { console.log(e) },
    cache: false,
    contentType: false,
    processData: false
  })
}