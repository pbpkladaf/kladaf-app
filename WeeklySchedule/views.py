from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import get_user_model
from django.core import serializers
from .models import Schedule
from .forms import ScheduleForm
import json

from .serializers import ScheduleSerializer
from rest_framework import generics

from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required(login_url='/loginApp/login/')
def index(request):
    monday = Schedule.objects.filter(monday=True, user=request.user).order_by('hour')
    tuesday = Schedule.objects.filter(tuesday=True, user=request.user).order_by('hour')
    wednesday = Schedule.objects.filter(wednesday=True, user=request.user).order_by('hour')
    thursday = Schedule.objects.filter(thursday=True, user=request.user).order_by('hour')
    friday = Schedule.objects.filter(friday=True, user=request.user).order_by('hour')
    saturday = Schedule.objects.filter(saturday=True, user=request.user).order_by('hour')
    sunday = Schedule.objects.filter(sunday=True, user=request.user).order_by('hour')
    response = {'monday': monday, 'tuesday': tuesday, 'wednesday': wednesday, 
                'thursday': thursday, 'friday': friday, 'saturday': saturday, 'sunday': sunday}
    return render(request, 'weeklyschedule_index.html', response)

@login_required(login_url='/loginApp/login/')
def addSchedule(request):
    form = ScheduleForm(request.POST or None)

    if (form.is_valid() and request.method == 'POST'):
        author = form.save(commit=False)
        author.user = request.user
        form.save()
        return HttpResponseRedirect('/weekly-schedule/')
    else:
        form = ScheduleForm()

    response = {'form' : form}
    return render(request, 'weeklyschedule_forms.html', response)

@login_required(login_url='/loginApp/login/')
# def deleteSchedule(request, id, day):
#     obj = Schedule.objects.get(id=id)
#     if (day=='monday'):
#         obj.monday=False
#         obj.save()
#     if (day=='tuesday'):
#         obj.tuesday=False
#         obj.save()
#     if (day=='wednesday'):
#         obj.wednesday=False
#         obj.save()
#     if (day=='thursday'):
#         obj.thursday=False
#         obj.save()
#     if (day=='friday'):
#         obj.friday=False
#         obj.save()
#     if (day=='saturday'):
#         obj.saturday=False
#         obj.save()
#     if (day=='sunday'):
#         obj.sunday=False
#         obj.save()

#     return HttpResponseRedirect('/weekly-schedule/')
def deleteSchedule(request, id, day):
    if (request.is_ajax() and request.method == "POST"):
        obj = Schedule.objects.get(id=id)
        if (day=='monday'):
            obj.delete()
            return JsonResponse({"message":"success"})
        if (day=='tuesday'):
            obj.delete()
            return JsonResponse({"message":"success"})
        if (day=='wednesday'):
            obj.delete()
            return JsonResponse({"message":"success"})
        if (day=='thursday'):
            obj.delete()
            return JsonResponse({"message":"success"})
        if (day=='friday'):
            obj.delete()
            return JsonResponse({"message":"success"})
        if (day=='saturday'):
            obj.delete()
            return JsonResponse({"message":"success"})
        if (day=='sunday'):
            obj.delete()
            return JsonResponse({"message":"success"})
    return JsonResponse({"message":"Wrong route"})

@login_required(login_url='/loginApp/login/')
def deleteAll(request):
    Schedule.objects.all().delete()

    return HttpResponseRedirect('/weekly-schedule/')

def get_schedule(request, id):
    data = Schedule.objects.filter(pk=id)
    return HttpResponse(serializers.serialize("json", data))

@csrf_exempt
def all_schedule_api(request):
    data = Schedule.objects.all()
    response = serializers.serialize("json", data)
    return HttpResponse(response, content_type='application/json')

@csrf_exempt
def add_schedule_api(request):
    if request.method == "POST":
        schedule_title = json.loads(request.body)
        print(schedule_title)
        activity = schedule_title['activity']
        hour = schedule_title['hour']
        detail = schedule_title['detail']
        message = schedule_title['message']

        res = serializers.serialize('json', hour.objects.all())
        print(res)
        return HttpResponse(res, content_type='application/json')
     
    else:
        return JsonResponse({'res':'must use post method'})

@csrf_exempt
def update_schedule_api(request,id):
    if request.method == "POST":
        schedule_title = json.loads(request.body)
        print(schedule_title)
        activity = schedule_title['activity']
        hour = schedule_title['hour']
        detail = schedule_title['detail']
        message = schedule_title['message']

        update = hour.objects.get(id=id)   
        update.activity = activity
        update.hour = hour
        update.detail = detail
        update.message = message
        update.save()

        res = serializers.serialize('json', hour.objects.all())
        print(res)
        return HttpResponse(res, content_type='application/json')
     
    else:
        return JsonResponse({'res':'must use post method'})