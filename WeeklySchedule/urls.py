from django.urls import path
from WeeklySchedule.views import index, addSchedule, deleteSchedule, deleteAll, get_schedule, all_schedule_api, add_schedule_api, update_schedule_api

app_name = "schedules"

urlpatterns = [
    path('', index, name='index'),
    path('add-schedule/', addSchedule, name='add-schedule'),
    # path('<id>/<day>/delete-schedule/', deleteSchedule, name='add-schedule'),
    path('delete-all/', deleteAll, name='delete-all'),
    path('<day>/<id>', deleteSchedule, name='delete-schedule'),
    path('get-schedule/', get_schedule, name='get-schedule'),
    path('all-schedule-api/', all_schedule_api, name='all-schedule-api'),
    path('add-schedule-api/', add_schedule_api, name='add-schedule-api'),
    path('update-schedule-api/', update_schedule_api, name='update-schedule-api')
]