from django.shortcuts import render, redirect
from django.views.decorators.http import require_GET, require_POST
from django.http import JsonResponse

from .models import WishList
from .forms import WishListForm

from .serializers import WishListSerializer
from rest_framework import generics

from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/loginApp/login/')
def indexWishList(request):
    wish_list = WishList.objects.order_by('id')
    form = WishListForm()

    response = {'wish_list' : wish_list, 'form' : form}
    return render(request, 'WishList_index.html', response)

@require_POST
@login_required(login_url='/loginApp/login/')
def addWishList(request):
    form = WishListForm()
    if request.is_ajax():
        form = WishListForm(request.POST)
        print(request.POST)
        if form.is_valid():
            new_wish = WishList(text=request.POST['text'])
            new_wish.save()
            return JsonResponse({
                'message': 'success'
            })
        
    return render(request, 'WishList_index.html', {'form': form})

@require_GET
@login_required(login_url='/loginApp/login/')
def viewAllWishList(request):
    
    return redirect('indexWishList')

@login_required(login_url='/loginApp/login/')
def completeWishList(request, wish_list_id):
    wish = WishList.objects.get(pk=wish_list_id)
    wish.complete = True
    wish.save()

    return redirect('indexWishList')

@login_required(login_url='/loginApp/login/')
def deleteCompletedWish(request):
    WishList.objects.filter(complete__exact=True).delete()

    return redirect('indexWishList')

@login_required(login_url='/loginApp/login/')
def deleteAllWish(request):
    WishList.objects.all().delete()

    return redirect('indexWishList')

class WishListAPI(generics.ListCreateAPIView):
    queryset = WishList.objects.all()
    serializer_class = WishListSerializer

class DetailWishListAPI(generics.RetrieveUpdateDestroyAPIView):
    queryset = WishList.objects.all()
    serializer_class = WishListSerializer