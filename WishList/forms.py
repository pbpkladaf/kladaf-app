from django import forms

class WishListForm(forms.Form):
    text = forms.CharField(max_length=50, 
        widget=forms.TextInput(
            attrs={
                'class' : 'form-control', 
                'placeholder' : 'Enter your wish', 
                'aria-label' : 'wishlist', 
                'aria-describedby' : 'add-btn'}))

                