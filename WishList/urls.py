from django.urls import path

from WishList.views import indexWishList, addWishList, indexWishList, viewAllWishList, completeWishList, deleteCompletedWish, deleteAllWish, DetailWishListAPI, WishListAPI

urlpatterns = [
    path('', indexWishList, name='indexWishList'),
    path('add', addWishList, name='addWishList'),
    path('view', viewAllWishList, name='viewWishList'),
    path('complete/<wish_list_id>', completeWishList, name='completeWishList'),
    path('deletecomplete', deleteCompletedWish, name='deletecompleteWishList'),
    path('deleteall', deleteAllWish, name='deleteallWishList'),
    path('apis',WishListAPI.as_view(), name='listAPI'),
    path('apis/<int:pk>', DetailWishListAPI.as_view(), name='detailListAPI'),
]