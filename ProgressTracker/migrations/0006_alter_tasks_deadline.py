# Generated by Django 3.2.8 on 2021-10-31 00:25

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ProgressTracker', '0005_auto_20211031_0003'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tasks',
            name='deadline',
            field=models.DateField(null=datetime.date.today),
        ),
    ]
