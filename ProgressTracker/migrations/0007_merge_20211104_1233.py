# Generated by Django 3.2.8 on 2021-11-04 05:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ProgressTracker', '0005_alter_tasks_status'),
        ('ProgressTracker', '0006_alter_tasks_deadline'),
    ]

    operations = [
    ]
