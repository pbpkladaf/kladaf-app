from typing import Any, ItemsView
from django.shortcuts import get_object_or_404, render
from ProgressTracker.forms import TasksForm
from django.http import HttpResponseRedirect, response
from ProgressTracker.models import Tasks
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponse
import json
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
@login_required(login_url='/loginApp/login/')
def index(request):
    no_status = Tasks.objects.filter(status="No Status", user=request.user)
    not_started = Tasks.objects.filter(status="Not Started", user=request.user)
    in_progress = Tasks.objects.filter(status="In Progress", user=request.user)
    in_review = Tasks.objects.filter(status="In Review", user=request.user)
    completed = Tasks.objects.filter(status="Completed", user=request.user)
    response = {'no_status':no_status, 'not_started':not_started, 'in_progress':in_progress,
                'in_review':in_review, 'completed':completed}
    return render(request, 'ProgressTracker_index.html', response)

@login_required(login_url='/loginApp/login/')
def add_tasks(request):
    form = TasksForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        author = form.save(commit=False)
        author.user = request.user
        form.save()
        return HttpResponseRedirect('/progress-tracker')
    else:
        form = TasksForm()

    context = {'form' : form}
    return render(request, 'ProgressTracker_form.html', context)

@login_required(login_url='/loginApp/login/')
def delete_tasks(request,id):
    task = Tasks.objects.get(id=id)
    task.delete()
    return HttpResponseRedirect('/progress-tracker')

@login_required(login_url='/loginApp/login/')
def update_tasks(request,id):  

    task = get_object_or_404(Tasks, id=id)
    form = TasksForm(request.POST or None, instance=task)

    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/progress-tracker')
    response = {'form' : form}
    return render(request, 'ProgressTracker_form.html',response)

@csrf_exempt
def all_tasks_api(request):
    task = Tasks.objects.all()
    response = serializers.serialize("json", task )
    return HttpResponse(response, content_type='application/json')

@csrf_exempt
def add_task_api(request):
    if request.method == "POST":
        task_title = json.loads(request.body)
        print(task_title)
        task= task_title['task']
        status = task_title['status']

        res = serializers.serialize('json', task.objects.all())
        print(res)
        return HttpResponse(res, content_type='application/json')
     
    else:
        return JsonResponse({'res':'must use post method'})

@csrf_exempt
def update_task_api(request,id):
    if request.method == "POST":
        task_title = json.loads(request.body)
        print(task_title)
        task= task_title['task']
        status = task_title['status']

        update = task.objects.get(id=id)   
        update.task = task
        update.status = status
        update.save()

        res = serializers.serialize('json', task.objects.all())
        print(res)
        return HttpResponse(res, content_type='application/json')
     
    else:
        return JsonResponse({'res':'must use post method'})




