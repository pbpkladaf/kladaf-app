from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Tasks(models.Model):
    add_tasks = models.CharField(max_length=100)
    status = models.CharField(max_length=30, default="No Status")
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)