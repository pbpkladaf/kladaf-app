import datetime
from django import forms
from .models import Tasks

class TasksForm(forms.ModelForm):
    class Meta:
        model = Tasks
        fields = ['add_tasks', 'status']

    add_tasks = forms.CharField(label='Add Task',required=True, widget=forms.TextInput(attrs={'type' : 'text', 'placeholder' : 'Add Your Tasks Here'}))