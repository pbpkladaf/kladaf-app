from django.urls import path
from django.conf.urls import url, include
from .views import add_tasks, delete_tasks, index, update_tasks, all_tasks_api, add_task_api, update_task_api

urlpatterns = [
    path('', index, name='index'),
    path('add-tasks', add_tasks),
    path('<id>/delete-tasks/', delete_tasks),
    path('<id>/update-tasks/', update_tasks, name='update_tasks'),
    path('all-tasks-api/', all_tasks_api, name='all_tasks_api'),
    path('add-task-api', add_task_api, name='add_task_api'),
    path('update-task-api', update_task_api, name='update_task_api'),
]
