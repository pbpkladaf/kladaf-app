"""kladaf URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
import ProgressTracker.urls as ProgressTrecker
import main.urls as main
import loginApp.urls as loginApp
import WishList.urls as WishList
import WeeklySchedule.urls as WeeklySchedule
import ProgressTracker.urls as ProgressTracker
import ReviewAndRating.urls as ReviewAndRating
import pomoTimer.urls as PodomoroTimer

urlpatterns = [
    path('admin/', admin.site.urls),
    path('loginApp/', include('loginApp.urls')),
    path('', include(loginApp)),
    path('', include(main)),
    path('diary/', include('Diary.urls')),
    path('wish-list/', include(WishList)),
    path('weekly-schedule/', include(WeeklySchedule)),
    path('progress-tracker/', include(ProgressTracker)),
    path('podomoro-timer/', include(PodomoroTimer)),
    path('rate/', include(ReviewAndRating)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)