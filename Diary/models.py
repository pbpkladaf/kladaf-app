from django.conf import settings
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User



class Diary(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    judul = models.CharField(default="", max_length=75, unique=True)
    tanggal = models.DateField(default=timezone.now)
    isi = models.CharField(default="", max_length=5000)

