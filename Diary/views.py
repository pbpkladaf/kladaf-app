from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse, JsonResponse
from django.core import serializers
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from rest_framework import viewsets, status
from rest_framework.decorators import api_view

from .forms import DiaryForm
from .models import Diary
from .serializers import DiarySerializer


@login_required(login_url='/loginApp/login/')
def index(request):
    diaries = Diary.objects.filter(user=request.user).values()
    response = {'diaries': diaries}
    return render(request, 'diary.html', response)


@login_required(login_url='/loginApp/login/')
def add_diary(request):
    context = {}
    form = DiaryForm(request.POST)
    if request.method == 'POST':
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = User.objects.get(pk=request.user.id)
            form.save()
            return HttpResponseRedirect('/diary')
    context['form'] = form
    return render(request, 'diary_form.html', context)


@login_required(login_url='/loginApp/login/')
def delete_diary(request, id):
    diary = Diary.objects.get(id=id)
    diary.delete()
    return HttpResponseRedirect('/diary')

    
@login_required(login_url='/loginApp/login/')
def delete_all_diary(request):
    Diary.objects.filter(user=request.user).delete()
    return HttpResponseRedirect('/diary')


@login_required(login_url='/loginApp/login/')
def read(request):
    diaries = Diary.objects.filter(user=request.user).values()
    context = {'diaries': diaries}
    return render(request, 'diary_card.html', context)


@login_required(login_url='/admin/login/')
def json(request):
    data = serializers.serialize('json', Diary.objects.all())
    return HttpResponse(data, content_type="application/json")


@api_view(['GET'])
def delete_flutter_all(request, pk):
    diary = Diary.objects.filter(user=pk)
    data = serializers.serialize('json', diary)
    if request.method == 'GET':
        diary.delete()
        return HttpResponse(data, content_type="application/json")


@api_view(['GET'])
def delete_flutter(request, pk):
    diary = Diary.objects.filter(id=pk)
    if request.method == 'GET':
        diary.delete()
        return JsonResponse({'message': 'Diary was deleted successfully!'})


class DiaryViewSet(viewsets.ModelViewSet):
    queryset = Diary.objects.all()
    serializer_class = DiarySerializer

