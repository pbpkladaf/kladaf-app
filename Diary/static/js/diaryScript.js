$("textarea").keyup(function () {
  var characterCount = $(this).val().length,
    current = $("#current");

  current.text(characterCount);
});

$("textarea").on("input", function () {
  this.style.height = "auto";

  this.style.height = this.scrollHeight + "px";
});

$(document).ready(function () {
  if ($("#cardView") != null) {
    Read();
  }

  $(document).on("click", ".delete", function () {
    $id = $(this).attr("name");
    $.ajax({
      url: "delete/" + $id,
      type: "POST",
      data: {
        csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
      },
      success: function () {
        Read();
        $(".modal").modal("hide");
        $("body").removeClass("modal-open");
        $(".modal-backdrop").remove();
      },
    });
  });
});

function Read() {
  $.ajax({
    url: "/diary/read",
    type: "POST",
    async: false,
    data: {
      res: 1,
      csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
    },
    success: function (response) {
      $("#cardView").html(response);
    },
  });
}

function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== "") {
    const cookies = document.cookie.split(";");
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === name + "=") {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

fetch("json", {
  method: "DELETE",
  credentials: "same-origin",
  headers: {
    "X-Requested-With": "XMLHttpRequest",
    "X-CSRFToken": getCookie("csrftoken"), // don't forget to include the 'getCookie' function
  },
})
  .then((response) => response.json())
  .then((data) => {
    console.log(data);
  });
