from django.urls import path
from .views import index, add_diary, delete_diary, delete_all_diary, json, read, DiaryViewSet, delete_flutter_all, \
    delete_flutter
from django.conf.urls import url, include
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'diarySet', DiaryViewSet)

urlpatterns = [
    path('', index, name='index'),
    path('add/', add_diary, name='add_diary'),
    path('delete-all/', delete_all_diary, name='delete-all'),
    path('json/', json, name='json'),
    url(r'^delete/(?P<id>\d+)$', delete_diary, name='delete-diary'),
    url(r'^read$', read, name='read'),
    url(r'^api/', include(router.urls)),
    # url(r'^api-auth/', include('rest_framework_urls', namespace='rest_framework'))
    url(r'^api/delete-all/(?P<pk>\d+)/$', delete_flutter_all),
    url(r'^api/delete-flutter/(?P<pk>\d+)/$', delete_flutter),
]
