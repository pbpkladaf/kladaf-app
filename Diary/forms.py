from django import forms
from .models import Diary


class DiaryForm(forms.ModelForm):
    class Meta:
        model = Diary
        fields = ('judul', 'isi')

        widgets = {
            'judul': forms.TextInput(
                attrs={'class': 'form-control'}),

            'isi': forms.Textarea(
                attrs={'class': 'form-control floatingTextArea'}),
        }
