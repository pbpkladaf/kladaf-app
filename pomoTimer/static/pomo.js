var minutes;
var seconds;
function podomoro() {
minutes = 25;
seconds = 0;
var interval = setInterval(
  function() {
    var elemen = document.getElementById("timer");
    if(seconds == 0 && minutes == 0) {
      clearInterval(interval);
      elemen.innerHTML = "00:00";
      elemen.style.backgroundColor = "red";
    } else if(seconds == 0) {
        minutes = minutes - 1;
        seconds = 59
        elemen.innerHTML = minutes + ":" + seconds;
    } else if(minutes > 0) {
        seconds = seconds -1;
        elemen.innerHTML = minutes + ":" + seconds;
    }
  },
  1000
)
}
function shortBreak() {
minutes = 5;
seconds = 0;
var interval = setInterval(
  function() {
    var elemen = document.getElementById("timer");
    if(seconds == 0 && minutes == 0) {
      clearInterval(interval);
      elemen.innerHTML = "00:00";
      elemen.style.backgroundColor = "red";
    } else if(seconds == 0) {
        minutes = minutes - 1;
        seconds = 59
        elemen.innerHTML = minutes + ":" + seconds;
    } else if(minutes > 0) {
        seconds = seconds -1;
        elemen.innerHTML = minutes + ":" + seconds;
    }
  },
  1000
)
}
function longBreak() {
minutes = 15;
seconds = 0;
var interval = setInterval(
  function() {
    var elemen = document.getElementById("timer");
    if(seconds == 0 && minutes == 0) {
      clearInterval(interval);
      elemen.innerHTML = "00:00";
      elemen.style.backgroundColor = "red";
    } else if(seconds == 0) {
        minutes = minutes - 1;
        seconds = 59
        elemen.innerHTML = minutes + ":" + seconds;
    } else if(minutes > 0) {
        seconds = seconds -1;
        elemen.innerHTML = minutes + ":" + seconds;
    }
  },
  1000
)
}
 
$(document).on("submit", "#form", function(e) {

  
  e.preventDefault();
  $.ajax({
     type:'POST',
     url: "addNewTask",
     data: {
         task:$('#input').val(),
         csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
     },
     success:function() {
         alert("Your task has been added!");
     }
 });

});
