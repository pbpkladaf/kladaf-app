from django.urls import path
from django.urls import include
from .views import get, createTask

urlpatterns = [
  path('', get, name='pomo_task_url'),
  path('addNewTask', createTask, name='addNewTask'),
]
