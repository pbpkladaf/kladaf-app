from django.http import response,JsonResponse
from django.shortcuts import redirect, render
from django.views.decorators.http import require_POST
from django.views.generic import View
# Create your views here.
from .models import Task
from .forms import TaskForm
def get(request):
  taskForm = TaskForm()
  task = Task.objects.all().values()
  response = {'task': task, 'form': taskForm}
  return render(request, 'pomo.html', response)

@require_POST
def createTask(request):
   task_form = TaskForm()
   if request.is_ajax():
       task_form = TaskForm(request.POST)
       print(request.POST)
       if task_form.is_valid():
           new_task = Task(task=request.POST['task'])
           new_task.save()
           return JsonResponse({
               'message': 'success'
           })
 
   task = Task.objects.all().values()
   response = {'task': task, 'form': task_form}
 
   return render(request, 'pomo.html', response)
  
