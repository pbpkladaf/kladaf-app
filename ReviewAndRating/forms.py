from django.forms import widgets
from ReviewAndRating.models import Rate
from django import forms

class RateForm(forms.Form):
    description = forms.CharField(
        widget=forms.Textarea
    )

    def save(self):
        rating = Rate.objects.create(description = self.cleaned_data['description'])

        return rating