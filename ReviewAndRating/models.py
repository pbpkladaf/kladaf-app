from django.db import models
from django.contrib.auth.models import User
from django.db.models.fields.related import ForeignKey

# Create your models here.
class Rate(models.Model):
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    star = models.IntegerField(default=1, null = True, blank=True)
    description = models.TextField(max_length=1000, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    # class Meta:
    #     uniq = ('user',)

