from django.contrib import admin
from .models import Rate

# Register your models here.

class RateAdmin(admin.ModelAdmin):
    list_display = ['star', 'description', 'created_at']
    readonly_fields = ['created_at', ]

admin.site.register(Rate, RateAdmin)
