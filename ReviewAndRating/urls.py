from django.urls import path
from .views import Review_rate, Hasil_Review, jsonResult, Rating_flutter

urlpatterns = [
    path('', Hasil_Review, name='Review'),
    path('rate/', Review_rate, name='Rate'),
    path('json/', jsonResult, name='Json'),
    path('flutter/', Rating_flutter)
]