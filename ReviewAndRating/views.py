import json
from django.views.decorators.csrf import csrf_exempt
from django.core.checks import messages
from django.http.response import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from django.http import HttpResponse
from ReviewAndRating.forms import RateForm
from ReviewAndRating.models import Rate
from django.core import serializers

# Create your views here.
def Review_rate(request):
    if request.method == 'POST' and request.is_ajax():
        form = RateForm(request.POST)
        if form.is_valid():
            data = form.save()
            print(request.POST)
            data.star = int(request.POST['rate'][0])
            data.save()
            return JsonResponse({ 'message' : 'Berhasil' })
            
    return render(request, 'Rating.html')

def Hasil_Review(request):
    review = Rate.objects.all().values()
    response = {'review' : review}
    return render(request, 'Review.html', response)

def jsonResult(request):
    result = serializers.serialize('json', Rate.objects.all())
    return HttpResponse(result, content_type="apllication/json")

@csrf_exempt
def Rating_flutter(request):
    if(request.method == 'POST'):
        print(request.method)
        print(request.body)

        data = json.loads(request.body)

        rate = data["rating"]
        desc = data["description"]
        date = data["date"]
        print(f"{rate} n {desc} n {date}")

        
        rating = Rate.objects.create(star = rate, description = desc, created_at = date)
        rating.save()
        print(rating)
        return JsonResponse({"status_code": 200})