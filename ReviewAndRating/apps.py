from django.apps import AppConfig


class ReviewandratingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ReviewAndRating'
