from django.db import models
from django.contrib.auth.models import User
# from PIL import Image

# Create your models here

class Customer(models.Model):
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=200, null=True)
    session_id = models.CharField(max_length=200, null=True)
    hobby = models.CharField(max_length=200, null=True)
    profile_pic = models.ImageField(default="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg", upload_to='profile_pics')
    # profile_pic = models.ImageField(default="candy.jpg", upload_to='profile_pics/')

    # def __str__(self):
    #     return self.name

    # def __str__(self):
    #     return f'{self.user.username} Customer'


