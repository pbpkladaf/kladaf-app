import json
import django
import secrets
# from django.contrib.auth.backends import UserModel
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, render, redirect 
from django.http import HttpResponse, request
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from django.contrib import messages

# from kladaf.settings import SECRET_KEY

# Create your views here.
from .models import *
from .forms import CreateUserForm,  CustomerForm, CreateProfileForm
from django.http import JsonResponse


def registerPage(request):

		form = CreateUserForm()
		if request.method == 'POST':
			form = CreateUserForm(request.POST)
			if form.is_valid():
				form.save()
				user = form.cleaned_data.get('username')
				group = Group.objects.get(name='customer')
				user.groups.add(group)

				# group = Group.objects.get(name='customer')
				# user.groups.add(group)

				messages.success(request, "Account was created!")

			# else:
				# messages.error(request, 'Account was not created')

				return redirect('/loginApp/login/')

		context = {'form':form}
		return render(request, 'register.html', context)

def loginPage(request):
		if request.method == 'POST':
			# form = AuthenticationForm(request, data=request.POST)
			# if form.is_valid():
			username = request.POST.get('username')
			password = request.POST.get('password')

			user = authenticate(request, username=username, password=password)

			if user is not None:
				login(request, user)
				return redirect('/loginApp/account/')
			else:
				messages.info(request, 'Username OR password is incorrect')

		context = {}
		return render(request, 'login.html', context)

def logoutUser(request):
	logout(request)
	messages.info(request, 'You are now logged out')
	return redirect('/loginApp/login/')

@login_required(login_url='/loginApp/login/')
def accountSettings(request):
	try:
		customer = request.user.customer
		# form = CustomerForm(instance=customer)
	except Customer.DoesNotExist:
	# customer = request.user.customer
		customer = Customer(user=request.user)
	
	form = CustomerForm(instance=customer)
# request.method == 'POST'
	if request.is_ajax and request.method == 'POST':
		form = CustomerForm(request.POST,request.FILES,instance=customer)
		if form.is_valid():
			form.save()
			
			return JsonResponse({
				'message': 'success'
			})


	return render(request, 'account_settings.html', {'form':form})

def createSession(username):
    data = {}
    user = User.objects.get(username=username)
    data['user'] = user
    data['user_id'] = user.pk
    data['session_id'] = secrets.token_urlsafe(50)
    return data

@csrf_exempt
def loginFlutter(request):
	# if request.method == "POST":
		# user_data = json.loads(request.body)
		# username = user_data['username']
		# password = user_data['password']

	data = {"status": "Login failed.", "success": False}
	if request.method == "POST":
		user_data = json.loads(request.body)
		username = user_data['username']
		password = user_data['password']

	# username = request.POST.get('username')
	# password = request.POST.get('password')

		user = authenticate(username=username, password=password)
		if user is not None:
			# if user.is_active:
			# 	login(request, user)
			# 	# Redirect to a success page.
			# 	return JsonResponse({
			# 	"status": True,
			# 	"username": request.user.username,
			# 	"message": "Successfully Logged In!"
			# 	}, status=200)
			# else:
			# 	return JsonResponse({
			# 	"status": False,
			# 	"message": "Failed to Login, Account Disabled."
			# 	}, status=401)
			session_id = secrets.token_urlsafe(50)
			user_session = Customer.objects.filter(user=user)
			user_session.update(session_id=session_id)
				
			data['session_id'] = session_id
			data['user_id'] = user.pk
			data['status'] = "Login success."
			data['success'] = True

	# else:
	# 	retrn JsonResponse({
	# 		"status": False,
	# 	"message": "Failed to Login, check your email/password."
	# 	}, status=401)
	return JsonResponse(data)

@csrf_exempt
def registerFlutter(request):
    # if request.method == 'POST':
    #     data = json.loads(request.body)

    #     username = data["username"]
    #     email = data["email"]
    #     password1 = data["password1"]

    #     newUser = UserModel.objects.create_user(
    #     username = username, 
    #     email = email,
    #     password = password1,
    #     )

    #     newUser.save()
    #     return JsonResponse({"status": "success"}, status=200)
    # else:
    #     return JsonResponse({"status": "error"}, status=401)
	data = {"status": "Registrasi gagal.", "success": False}
	if request.method == "POST":
		_data = json.loads(request.body)
		form = CreateUserForm(_data)
		if form.is_valid():
			form.save()
			session_data = createSession(_data['username'])
			profile = CreateProfileForm(session_data)
			if profile.is_valid():
				profile.save()
			data['status'] = "Registrati berhasil"
			data['session_id'] = session_data['session_id']
			data['user_id'] = session_data['user_id']
			data['success'] = True
	return JsonResponse(data)


@csrf_exempt
def logoutFlutter(request):
    try:
        logout(request)
        return JsonResponse({
                    "status": True,
                    "message": "Successfully Logged out!"
                }, status=200)
    except:
        return JsonResponse({
          "status": False,
          "message": "Failed to Logout"
        }, status=401)

@csrf_exempt
def editProfileFlutter(request):
	data = {}
	if request.method == "POST":
		data = json.loads(request.body)
		form = CustomerForm(data)

		# customer = Customer(user=request.user)
		user = User.objects.get(pk=data['user_id'])
		customer = Customer.objects.get(user=user)

		if form.is_valid():
			customer.name = data['name']
			customer.hobby = data['hobby']
			customer.save()
			
			data['message'] = "Berhasil mengganti profil."
			data['success'] = True
		else:
			data['message'] = "Mohon masukkan data yang benar"
	return JsonResponse(data)
    # try :
    #     user = get_object_or_404(User,id=id)
    # except:
    #     print("errorr")

        # user.save()
    #     print("test")
    #     return JsonResponse({"data":"Profile has been updated", "status": 200}, status=200)
    
    # context["data"] = {
    #     "nama" : user.nama,
    #     "hobby" : user.hobby,
    # }
    # print("tes2222")
    # return JsonResponse({"data":context, "status": 200}, status=200)

@csrf_exempt
def auth(request):
    data = {"is_authenticated": False}
    if request.method == "POST":
        session_data = json.loads(request.body)
        user_id = session_data['user_id']
        user = User.objects.get(pk=user_id)
        user_session = Customer.objects.get(user=user)
        if user_session.session_id == session_data['session_id']:
            data['is_authenticated'] = True
    return JsonResponse(data)


@csrf_exempt
def getData(request, id):
	data = {'success': False}
	if request.method == "GET":
		user = User.objects.get(pk=id)
		print(user)
		customer = Customer.objects.get(user=user)
		# data['name'] = user.username
		data['name'] = customer.name
		# data['name'] = customer.name
		data['hobby'] = customer.hobby
		data['success'] = True
	return JsonResponse(data)
