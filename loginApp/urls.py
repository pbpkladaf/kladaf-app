from django.urls import path
from . import views

urlpatterns = [
    path('login/', views.loginPage, name='login'),
    path('register/', views.registerPage, name='register'),
    path('account/', views.accountSettings, name='account'),
    path('logout/', views.logoutUser, name='logout'),

    path('loginflutter/', views.loginFlutter, name='loginFlutter'),
    path('registerflutter/',views.registerFlutter,name='registerFlutter'),
    path('auth/', views.auth, name='auth'),
    path('logoutflutter/',views.logoutFlutter,name='logoutFlutter'),
    path('editprofile/',views.editProfileFlutter,name='editprofile'),
    path('getData/<id>', views.getData, name='getData'),
]